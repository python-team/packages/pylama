pylama (8.4.1-2) unstable; urgency=medium

  * Team upload.
  * Add missing runtime dependency on python3-pkg-resources

 -- Alexandre Detiste <tchet@debian.org>  Sat, 18 Jan 2025 19:55:36 +0100

pylama (8.4.1-1) unstable; urgency=medium

  * New upstream version 8.4.1
  * Remove pylama.egg-info/ to fix build
  * Remove d/patches/python37.patch as bug fixed upstream
  * Update d/watch version
  * Update Standards-Version: 4.2.1 -> 4.6.2: no change needed
  * d/copyright: update license to MIT
  * d/tests/: add a minimal autopkgtests test
  * Fix "AttributeError: module 'pyflakes.messages' has no attribute
    'RedefinedInListComp'" using new upstream version (Closes: #1041344)

 -- Ludovic Rousseau <rousseau@debian.org>  Mon, 31 Jul 2023 20:09:25 +0200

pylama (7.4.3-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + Build-Depends: Drop versioned constraint on python3-pydocstyle and
      python3-pyflakes.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 16 Nov 2022 10:59:25 +0000

pylama (7.4.3-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on pycodestyle and pydocstyle.

 -- Sandro Tosi <morph@debian.org>  Thu, 02 Jun 2022 21:23:58 -0400

pylama (7.4.3-3) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Thu, 25 Jul 2019 20:16:12 +0200

pylama (7.4.3-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Mattia Rizzolo ]
  * Bump Standards-Version to 4.2.1.
    + Set Rules-Requires-Root:no.
  * Add patch from upstream for compatibility with Python 3.7.  Closes: #904491

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 26 Sep 2018 11:53:12 +0200

pylama (7.4.3-1) unstable; urgency=medium

  * New upstream release.
  * Merge to unstable.
  * Bump Standards-Version to 4.1.3
    * Use https in Format in debian/copyright.
  * Bump compat to 11.
  * Update Vcs-* fields to salsa.debian.org.
  * Remove unused README.Debian.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Tue, 13 Feb 2018 07:32:24 +0800

pylama (7.4.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.0.0.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Mon, 03 Jul 2017 16:26:09 +0800

pylama (7.3.3-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Source only upload to dependencies (Closes: #856794).

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 06 May 2017 17:38:42 +0200

pylama (7.3.3-1) unstable; urgency=medium

  * New upstream release.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Mon, 02 Jan 2017 02:20:09 +0800

pylama (7.3.1-1) unstable; urgency=low

  * Initial release. Closes: #779449

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 14 Dec 2016 13:05:26 +0800
