= PYLAMA(1)
:doctype: manpage

== Name

pylama - code audit tool for Python

== Synopsys

*pylama* [_-h_] [_--verbose_] [_--version_] [_--format {pep8, pylint}_] [_--select SELECT_] [_--sort SORT_] [_--linters LINTERS_] [_--ignore IGNORE_] [_--skip SKIP_] [_--report REPORT_] [_--hook_] [_--async_] [_--options OPTIONS_] [_--force_] [_--abspath_] [_paths_ [_paths ..._]]

== Options

*-h*, *--help*::
    show the help message and exit

*-v*, *--verbose*::
    verbose mode

*--version*::
    show program's version number and exit

*-f {pep8, pylint}*, *--format {pep8, pylint}*::
    Choose errors format (pep8, pylint).

*-s SELECT*, *--select SELECT*::
    Select errors and warnings. (comma-separated list)

*--sort SORT*::
    Sort result by error types. Ex. E, W, D

*-l LINTERS*, *--linters LINTERS*::
    Select linters. (comma-separated). Choices are pyflakes, pep8, pep257, mccabe.

 *-i IGNORE*, *--ignore IGNORE*::
     Ignore errors and warnings. (comma-separated)

*--skip SKIP*::
    Skip files by masks (comma-separated, Ex. **/messages.py*)

*-r REPORT*, *--report REPORT*::
    Send report to file [REPORT]

*--hook*::
   Install Git (Mercurial) hook.

*--async*::
    Enable async mode. Useful for checking a lot of files. Unsupported with pylint.

*-o FILE*, *--options FILE*::
    Specify configuration file. Looks for pylama.ini, setup.cfg, tox.ini, or
    pytest.ini in the current directory.

*-F*, *--force*::
    Force code checking (if linter doesn't allow)

*-a*, *--abspath*::
    Use absolute paths in output.

== Author

This manual page was written by mailto:czchen@debian.org[ChangZhuo Chen
'<czchen@debian.org>'] for the *Debian GNU/Linux system* (but may be used by
others).
